package cz.markaos.sharedchests

import org.bukkit.plugin.PluginLogger
import org.bukkit.plugin.java.JavaPlugin

class SharedEnderChest : JavaPlugin() {

    override fun onEnable() {
        super.onEnable()

        InventoryHelper.dataDirectory = dataFolder.absolutePath
        InventoryHelper.load()
        server.pluginManager.registerEvents(OpenListener(), this)
    }

    override fun onDisable() {
        super.onDisable()
        InventoryHelper.save()
    }
}