package cz.markaos.sharedchests

import net.kyori.adventure.text.Component
import org.bukkit.Bukkit
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.Inventory
import org.bukkit.plugin.PluginLogger
import java.io.File
import java.util.logging.Level

class InventoryHelper {

    companion object {
        val inventory: Inventory = Bukkit.createInventory(null, InventoryType.ENDER_CHEST, Component.text("Ender Chest"))
        var dataDirectory = ""
        var logger: PluginLogger? = null

        private fun open(): File {
            val dir = File(dataDirectory)
            dir.mkdirs()

            return File(dataDirectory + File.separator + "contents")
        }

        fun load() {
            inventory.clear()
            val file = open()

            logger?.log(Level.INFO, "Attempting to load from " + file.absolutePath)
            if (file.exists()) {
                inventory.contents = InventorySerializer.itemStackArrayFromBase64(file.readText())
            }
        }

        fun save() {
            val serialized = InventorySerializer.itemStackArrayToBase64(inventory.contents)
            val file = open()
            logger?.log(Level.INFO, "Attempting to save to " + file.absolutePath)
            file.delete()
            file.createNewFile()
            file.writeText(serialized)
        }
    }
}