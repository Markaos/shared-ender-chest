package cz.markaos.sharedchests

import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.world.WorldSaveEvent


class OpenListener : Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    fun onPlayerInteract(event: PlayerInteractEvent) {
        if (event.action == Action.RIGHT_CLICK_BLOCK && event.clickedBlock?.type == Material.ENDER_CHEST) {
            event.isCancelled = true
            event.player.openInventory(InventoryHelper.inventory)
        }
    }

    @EventHandler
    fun onWorldSave(event: WorldSaveEvent) {
        InventoryHelper.save()
    }

}